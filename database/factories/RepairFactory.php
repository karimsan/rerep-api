<?php

use Faker\Generator as Faker;

$factory->define(App\Repair::class, function (Faker $faker) {
    static $repairs = [
        'Engine',
        'Gearbox',
        'Wheel',
        'Bodywork',
        'Windshield',
        'Oil change',
        'Breaks check',
        'Car wash',
    ];

    $state = $faker->randomElement(['Complete', 'Incomplete']);
    $approved = $state === 'Incomplete' ? false : $faker->boolean;

    return [
        'name' => $faker->randomElement($repairs),
        'state' => $state,
        'approved' => $approved,
        'date' => $faker->dateTimeBetween('-1 month'),
        'assignee_id' => $faker->boolean(80) ? App\User::orderBy(DB::raw('RAND()'))->get()->first()->id : null
    ];
});

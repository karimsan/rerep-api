<?php

namespace App\Service;

use App\Repair;
use App\User;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class RepairService
{
    /**
     * Checks that new repair (taking 1 hour) doesn't overlap with eny existing
     * @param string $dateTime
     * @param int|null $id pass Repair id if checking for existing repair or null for new one
     * @throws ValidationException
     */
    public function checkForNoOverlap($dateTime, $id = null)
    {
        $dt = Carbon::parse($dateTime);
        $left = $dt->copy()->subHour();
        $right = $dt->copy()->addHour();

        $builder = Repair::where('date', '>', $left->toDateTimeString())
            ->where('date', '<', $right->toDateTimeString())
            ->when($id, function ($builder) use ($id) {
                $builder->where('id', '!=', $id);
            });

        if (($repair = $builder->first()) !== null) {
            throw ValidationException::withMessages(
                ['date' => ['Overlap with another repair started at '.$repair->date]]
            );
        }
    }

    /**
     * Store new repair in DB
     *
     * @param array $data
     * @return Repair
     * @throws ValidationException
     */
    public function create($data)
    {
        $this->checkForNoOverlap($data['date']);
        if ($data['state'] == Repair::STATE_COMPLETE) {
            $data['approved'] = true;
        }

        $repair = Repair::create($data);

        return $repair->refresh()->load('assignee');
    }

    /**
     * Update existing repair
     *
     * @param Repair $repair
     * @param User $user
     * @param array $data
     * @return Repair
     * @throws ValidationException
     */
    public function update(Repair $repair, User $user, $data)
    {
        if ($user->isManager()) {
            if (isset($data['state'])) {
                $data['approved'] = ($data['state'] == Repair::STATE_COMPLETE);
            }
            if (isset($data['date'])) {
                $this->checkForNoOverlap($data['date'], $repair->id);
            }
        } else {
            $data = array_only($data, 'state');
            if (($data['state'] ?? null) != Repair::STATE_COMPLETE) {
                throw ValidationException::withMessages(
                    ['state' => ['User can only mark repair as complete']]);
            }
        }

        $repair->update($data);

        return $repair->refresh()->load('assignee');
    }
}
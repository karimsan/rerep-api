<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'repair_id', 'comment'];
    protected $visible = ['id', 'comment', 'user', 'repair', 'created_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function repair()
    {
        return $this->belongsTo('App\Repair');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->toAtomString();
    }
}

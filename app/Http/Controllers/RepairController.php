<?php

namespace App\Http\Controllers;

use App\Repair;
use App\Service\RepairService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class RepairController extends Controller
{
    /**
     * @var RepairService
     */
    private $repairService;

    public function __construct(RepairService $repairService)
    {
        $this->repairService = $repairService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user->isUser()) {
            $repairs = Repair::with('assignee')->where('assignee_id', $user->id)->get();
        } else {
            $repairs = Repair::with('assignee')->get();
        }

        return response()->json(['repairs' => $repairs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $this->authorize('create', Repair::class);
        $data = $request->validate([
            'assignee_id' => 'nullable|integer|exists:users,id',
            'name'        => 'required|string|max:255',
            'date'        => 'required|date',
            'state'       => 'required|in:Complete,Incomplete',
        ]);

        $repair = $this->repairService->create($data);

        return response()->json(['repair' => $repair])->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Return repair data
     *
     * @param  Repair $repair
     * @return JsonResponse
     */
    public function show(Repair $repair)
    {
        $this->authorize('view', $repair);
        $repair->load('assignee');

        return response()->json(['repair' => $repair]);
    }

    /**
     * Update any field of repair (including state, approved flag)
     *
     * @param  Request $request
     * @param  Repair $repair
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, Repair $repair)
    {
        $this->authorize('update', $repair);
        $data = $request->validate([
            'assignee_id' => 'integer|exists:users,id',
            'name'        => 'string|max:255',
            'date'        => 'date',
            'state'       => 'in:Complete,Incomplete',
            'approved'    => 'boolean',
        ]);
        /** @var \App\User $user */
        $user = Auth::user();
        $repair = $this->repairService->update($repair, $user, $data);

        return response()->json(['repair' => $repair]);
    }

    /**
     * Remove existing repair
     *
     * @param  Repair $repair
     * @return JsonResponse
     */
    public function destroy(Repair $repair)
    {
        $this->authorize('delete', $repair);
        $repair->delete();

        return response()->json()->setStatusCode(Response::HTTP_ACCEPTED);
    }
}

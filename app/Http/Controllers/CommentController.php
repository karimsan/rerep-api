<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Repair;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Return list of comments attached to certain repair
     *
     * @param int $repairId
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function list($repairId)
    {
        $repair = Repair::findOrFail($repairId);
        $this->authorize('view', $repair);

        $comments = $repair->comments()->with('user')->get();

        return response()->json(['comments' => $comments]);
    }

    /**
     * Add new comment to repair
     * @param int $repairId
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function add($repairId, Request $request)
    {
        $repair = Repair::findOrFail($repairId);
        $this->authorize('view', $repair);

        $request->validate(['comment' => 'required|string']);
        $data = [
            'repair_id' => $repair->id,
            'user_id'   => Auth::user()->id,
            'comment'   => $request->get('comment'),
        ];

        $comment = Comment::create($data);
        $comment->refresh()->load('user');

        return response()->json(['comment' => $comment])->setStatusCode(Response::HTTP_CREATED);
    }
}

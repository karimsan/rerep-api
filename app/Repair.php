<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    const STATE_COMPLETE = 'Complete';
    const STATE_INCOMPLETE = 'Incomplete';

    // Fields accepted as input for create and update
    protected $fillable = ['name', 'assignee_id', 'date', 'state', 'approved'];

    // Fields exposed via API
    protected $visible = ['id', 'name', 'assignee', 'date', 'state', 'approved'];

    protected $casts = [
        'approved' => 'boolean',
    ];

    /**
     * Repair might be assigned to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Repair has comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment')->orderBy('created_at');
    }

    /**
     * Add timezone value to date response
     * @param $value
     * @return string
     */
    public function getDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->toAtomString();
    }
}

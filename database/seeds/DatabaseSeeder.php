<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Manager First',
            'email' => 'manager@example.org',
            'password' => bcrypt('toptal'),
            'role' => 'manager'

        ]);
        App\User::create([
            'name' => 'User Second',
            'email' => 'user@example.org',
            'password' => bcrypt('toptal'),
            'role' => 'user'

        ]);
        factory(App\User::class, 15)->create();
        factory(App\Repair::class, 100)->create();
        factory(App\Comment::class, 500)->create();
    }
}

<?php

namespace App\Http\Controllers;

use App\Repair;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:crud-users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = User::get();

        return response()->json(['users' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'role'     => 'required|in:'.User::ROLE_USER.','.User::ROLE_MANAGER,
        ]);


        $data = $request->only('name', 'email', 'password', 'role');
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);

        return response()->json(['user' => $user])->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return JsonResponse
     */
    public function show(User $user)
    {
        return response()->json(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  User $user
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name'     => 'string|max:255|required_without_all:email,password,role',
            'email'    => 'email|max:255|required_without_all:name,password,role|unique:users,email,'.$user->id,
            'password' => 'nullable|string|min:6|required_without_all:name,email,role',
            'role'     => 'in:'.User::ROLE_USER.','.User::ROLE_MANAGER.'|required_without_all:name,email,password',
        ]);

        $data = $request->only('name', 'email', 'password', 'role');
        if (array_key_exists('password', $data)) {
            if (empty($data['password'])) {
                // leave old password
                unset($data['password']);
            } else {
                // set new password
                $data['password'] = bcrypt($data['password']);
            }
        }

        $user->update($data);

        return response()->json(['user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return JsonResponse
     */
    public function destroy(User $user)
    {
        Repair::where('assignee_id', $user->id)
            ->update(['assignee_id' => null]);
        $user->delete();

        return response()->json()->setStatusCode(Response::HTTP_ACCEPTED);
    }
}

<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    $repair = App\Repair::orderBy(DB::raw('RAND()'))->get()->first();
    $userId = ($repair->assignee_id !== null && $faker->boolean)
        ? $repair->assignee_id
        : App\User::orderBy(DB::raw('RAND()'))
            ->where('role', 'manager')
            ->get()->first()->id;

    return [
        'comment'    => $faker->realText(),
        'user_id'    => $userId,
        'repair_id'  => $repair->id,
        'created_at' => $faker->dateTimeBetween('-1 week'),
    ];
});

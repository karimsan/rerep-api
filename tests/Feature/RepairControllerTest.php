<?php

namespace Tests\Feature;

use App\Repair;
use App\User;
use Carbon\Carbon;
use Faker\Factory as FakerFactory;
use Tests\TestCase;

class RepairControllerTest extends TestCase
{
    const BASE_URI = '/api/repairs';
    const DATE_FORMAT = 'Y-m-d H:i:s';

    protected function createUserAndGetAuthHeader($role = User::ROLE_USER)
    {
        $faker = FakerFactory::create();
        $password = $faker->password;
        $data = [
            'name'     => $faker->name,
            'email'    => $faker->safeEmail,
            'password' => bcrypt($password),
            'role'     => $role,
        ];
        $user = new User($data);
        $user->save();
        $token = \Auth::guard()->attempt(['email' => $data['email'], 'password' => $password]);

        return ['header' => ['Authorization' => 'Bearer '.$token], 'user' => $user];
    }

    protected function createManagerAndGetAuthHeader()
    {
        return $this->createUserAndGetAuthHeader(User::ROLE_MANAGER);
    }

    protected function generateRepairData($input = [])
    {
        $faker = FakerFactory::create();

        return $input + [
                'name'  => $faker->name,
                'date'  => $faker->dateTimeBetween('-1 month')->format(self::DATE_FORMAT),
                'state' => $faker->randomElement([Repair::STATE_COMPLETE, Repair::STATE_INCOMPLETE]),
            ];
    }

    public function testAuthRequired()
    {
        $response = $this->getJson(self::BASE_URI);

        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testUserCanNotCreateRepair()
    {
        $response = $this->postJson(self::BASE_URI, $this->generateRepairData(), $this->createUserAndGetAuthHeader()['header']);
        $response->assertStatus(403);
    }

    public function testManagerCanCreateRepair()
    {
        $response = $this->postJson(self::BASE_URI, $this->generateRepairData(), $this->createManagerAndGetAuthHeader()['header']);
        $response->assertStatus(201);
    }

    public function testUserCanViewAssignedRepair()
    {
        $userData = $this->createUserAndGetAuthHeader();
        $response = $this->postJson(self::BASE_URI, $this->generateRepairData(['assignee_id' => $userData['user']->id]), $this->createManagerAndGetAuthHeader()['header']);
        $response->assertStatus(201);
        $repair = $response->decodeResponseJson()['repair'];

        $response = $this->getJson(self::BASE_URI."/".$repair['id'], $userData['header']);
        $response->assertStatus(200);
        $this->assertEquals($repair['name'], $response->decodeResponseJson()['repair']['name']);
    }

    public function testTwoRepairsCanNotOverlap()
    {
        $date = Carbon::parse('-30 minutes');
        $response = $this->postJson(self::BASE_URI, $this->generateRepairData(['date' => $date->format(self::DATE_FORMAT)]), $this->createManagerAndGetAuthHeader()['header']);
        $response->assertStatus(201);

        $date->addMinutes(30);
        $response = $this->postJson(self::BASE_URI, $this->generateRepairData(['date' => $date->format(self::DATE_FORMAT)]), $this->createManagerAndGetAuthHeader()['header']);
        $response->assertStatus(422);

        $this->assertEquals("Overlap with another repair started at ".$date->subMinutes(30)->toAtomString(), $response->decodeResponseJson()['errors']['date'][0]);
    }
}

<?php

namespace App\Policies;

use App\User;
use App\Repair;
use Illuminate\Auth\Access\HandlesAuthorization;

class RepairPolicy
{
    use HandlesAuthorization;

    /**
     * Manager can access all CRUD operations for repairs
     *
     * @param User $user
     * @param string $ability
     * @return bool|null
     */
    public function before($user, $ability)
    {
        if ($user->isManager()) {
            return true;
        }
    }

    /**
     * User can view only assigned to him repair
     *
     * @param  User  $user
     * @param  Repair  $repair
     * @return mixed
     */
    public function view(User $user, Repair $repair)
    {
        return $user->id === $repair->assignee_id;
    }

    /**
     * User can't create new repair
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * User can update assigned repair (to mark as completed)
     *
     * @param  \App\User  $user
     * @param  \App\Repair  $repair
     * @return mixed
     */
    public function update(User $user, Repair $repair)
    {
        return $user->id === $repair->assignee_id;
    }

    /**
     * User can't delete any repair
     *
     * @param  \App\User  $user
     * @param  \App\Repair  $repair
     * @return mixed
     */
    public function delete(User $user, Repair $repair)
    {
        return false;
    }
}
